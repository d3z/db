SOURCES=main.c
OUTPUT=db
CC=clang

$(OUTPUT): build

build: $(SOURCES)
	$(CC) -O2 -o $(OUTPUT) $(SOURCES)

test: $(OUTPUT)
	bundle exec rspec

clean:
	rm $(OUTPUT)
